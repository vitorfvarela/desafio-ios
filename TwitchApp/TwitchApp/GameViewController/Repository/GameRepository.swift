//
//  GameRepository.swift
//  TwitchApp
//
//  Created by Vitor Ferraz on 25/04/2018.
//  Copyright © 2018 Vitor Ferraz. All rights reserved.
//

import Foundation
protocol GameService{
    typealias GameServiceCompletionHandler = ([Game], ErrorAPI?) -> Void
    func getAll(offset:Int, completionHandler:@escaping GameServiceCompletionHandler)
    
}

class GameRepository:GameService{
    private let api = RequestManagerImp.shared
    func getAll(offset: Int, completionHandler: @escaping GameService.GameServiceCompletionHandler) {
        let url = Endpoint.baseURL.text + "kraken/games/top?limit=31&offset=\(offset)"
        api.request(with: url, method: .get, params: nil) { (response, error) in
            if let error = error {
                completionHandler([], error)
                return
            }
            guard let json = response ,let data = json.array("top") else {
                completionHandler([], error)
                return
            }
            //Success
            completionHandler(data.compactMap(Game.init), nil)
        }
    }
}
