//
//  GameTableViewCell.swift
//  TwitchApp
//
//  Created by Vitor Ferraz on 25/04/2018.
//  Copyright © 2018 Vitor Ferraz. All rights reserved.
//

import UIKit
import Kingfisher

class GameTableViewCell: UITableViewCell {

    @IBOutlet weak var rankImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var channelLabel: UILabel!
    @IBOutlet weak var coverImage: UIImageView!
    @IBOutlet weak var rankLabel: UILabel!
    var index = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setup(with model:GamePresenterCellProtocol){
        self.titleLabel.text = model.titleCell
        self.channelLabel.text = model.channelCell
        self.rankLabel.text = model.rankCell
        guard let imageURL = model.setupImageCell() else{
            self.coverImage.image =  UIImage(named: "noImage")
            return
        }
            self.coverImage.kf.setImage(with: imageURL, placeholder: UIImage(named: "noImage"), options: [.transition(.fade(0.2))], progressBlock: nil, completionHandler: nil)
        
        switch model.indexCell {
        case 0:
            self.rankImage.image = #imageLiteral(resourceName: "top1")
        case 1:
            self.rankImage.image = #imageLiteral(resourceName: "top2")
        case 2:
            self.rankImage.image = #imageLiteral(resourceName: "top3")

        default:
            self.rankImage.image = #imageLiteral(resourceName: "noTop")
            
        }
    }

}
