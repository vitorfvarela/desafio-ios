//
//  GameViewController.swift
//  TwitchApp
//
//  Created by Vitor Ferraz on 25/04/2018.
//  Copyright © 2018 Vitor Ferraz. All rights reserved.
//

import UIKit

class GameViewController: UIViewController,UISearchResultsUpdating {
    
    

    @IBOutlet weak var gameTableView: UITableView!
    let refresh = UIRefreshControl()

    let searchController = UISearchController(searchResultsController: nil)

    private var dataSource:TableViewDataSource<GameTableViewCell, GamePresenterCellProtocol>!{
        didSet{
            self.gameTableView.dataSource = dataSource
            self.gameTableView.reloadData()
            self.gameTableView.delegate = self

        }
    }
    private var gamePresenter:GamePresenter!
    override func viewDidLoad() {
        super.viewDidLoad()
        gamePresenter = GamePresenter(view: self, repository: GameRepository())
        gamePresenter.loadGames()
        setupSearchBar()
        setupRefresh()
        

    }
    func updateSearchResults(for searchController: UISearchController) {
        print(searchController.searchBar.text!)
        let status =  searchController.searchBar.text! == "" ? false : true
        self.gamePresenter.searchGame(with: searchController.searchBar.text!, status: status)
    }
    
    func setupRefresh() {
        refresh.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        
        if #available(iOS 10.0, *) {
            gameTableView.refreshControl = refresh
        }
        
        gameTableView.sendSubview(toBack: refresh)
    }
    
    @objc func refreshData() {
         self.refresh.beginRefreshing()
        gamePresenter.loadGames(fromStart: true)
        
    }
    func setupSearchBar(){
        self.navigationController?.navigationBar.barStyle = UIBarStyle.black
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        searchController.searchBar.keyboardAppearance = UIKeyboardAppearance.default
        searchController.searchResultsUpdater = self
        searchController.searchBar.setValue(TitleGlossary.cancel.text, forKey:"_cancelButtonText")
        definesPresentationContext = true
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.white], for: .normal)
        
        if #available(iOS 11.0, *) {
            self.navigationController?.navigationBar.prefersLargeTitles = true
            searchController.obscuresBackgroundDuringPresentation = false
            navigationItem.searchController = searchController
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToDetail", let game = sender as? Game{
            guard let destinationVC = segue.destination as? DetailViewController else{
                fatalError("Erro ao criar view controller")
            }
            destinationVC.game = game
            
        }
    }
   
}

extension GameViewController:UITableViewDelegate{
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        Animation.animate(cell: cell)
        if indexPath.row == self.gamePresenter.allGames.count - 10 && !self.gamePresenter.isSearching{
            self.gamePresenter.loadGames()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let game = gamePresenter.getGame(index: indexPath.row)
        performSegue(withIdentifier: "goToDetail", sender: game)
    }
}

extension GameViewController:GamePresenterProtocol{
    func updateWithItems(with items: [Game]) {
        self.refresh.endRefreshing()
        
        self.dataSource = TableViewDataSource(cellIdentifier: "GameTableViewCell", items: items, configureCell: { (cell, model) in
                       
            cell.setup(with: model)
            cell.alpha = 0.0
        })
        
    }
    
}


