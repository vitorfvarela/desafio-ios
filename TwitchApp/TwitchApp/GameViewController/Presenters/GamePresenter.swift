//
//  GamePresenter.swift
//  TwitchApp
//
//  Created by Vitor Ferraz on 25/04/2018.
//  Copyright © 2018 Vitor Ferraz. All rights reserved.
//

import Foundation

protocol GamePresenterProtocol:BaseView {
    func updateWithItems(with items:[Game])
    
}



class GamePresenter:BasePresenter{
   
    
    internal var view: GamePresenterProtocol
    internal var repository: GameRepository
    
    typealias R = GameRepository
    typealias V = GamePresenterProtocol
    
    init(view:GamePresenterProtocol, repository:GameRepository) {
        self.repository = repository
        self.view = view
    }
    
    private var page = 1
    
     var allGames = [Game]()
     var filteredGames = [Game]()

     var isSearching = false
    
    func loadGames(fromStart:Bool = false){
        if fromStart{
            self.page = 1
            self.allGames.removeAll()
        }
        view.showLoading()
        repository.getAll(offset: page) { (gameList, error) in
            self.view.hideLoading()
            var items = [Game]()
            for (index, game) in gameList.enumerated() {
                var game = game
                game.setRank(rank: index)
                items.append(game)
            }
            if let error = error {
                self.view.showError(withMessage: error.messageError)
            }
            if !gameList.isEmpty{
                self.allGames += items
                self.page += 1
                self.view.updateWithItems(with: self.allGames)
            }
            return
            

            
        }
    }
    
    func searchGame(with query:String, status:Bool){
        self.isSearching = status
        if isSearching{
            self.filteredGames = self.allGames.filter{
                ($0.details?.name.lowercased().contains(query.lowercased()))!
                
            }
            self.view.updateWithItems(with: self.filteredGames)
        }else{
            self.view.updateWithItems(with: self.allGames)

        }
        
 

    }
    
    func getGame(index:Int)->Game{
        if isSearching{
            return filteredGames[index]
        }else{
            return allGames[index]
        }
    }
    
}
