//
//  AboutMeViewController.swift
//  TwitchApp
//
//  Created by Vitor Ferraz on 27/04/2018.
//  Copyright © 2018 Vitor Ferraz. All rights reserved.
//

import UIKit

class AboutMeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func linkedinButtonPressed(_ sender: Any) {
        openSite("https://www.linkedin.com/in/vitor-ferraz-ba143466")
    }
    
    @IBAction func githubButtonPressed(_ sender: Any) {
        openSite("https://github.com/VitorFerraz")
    }
    
    @IBAction func mediumButtonPressed(_ sender: Any) {
        openSite("https://medium.com/@vitorfvarela")
    }
    
    func openSite(_ baseURL: String) {

   
        guard let url = URL(string: baseURL) else{
            fatalError("Erro ao criar url")
        }
        UIApplication.shared.open(url, options: [:])
        
    }
}
