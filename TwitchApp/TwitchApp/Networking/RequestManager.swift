//
//  RequestManager.swift
//  TwitchApp
//
//  Created by Vitor Ferraz on 25/04/2018.
//  Copyright © 2018 Vitor Ferraz. All rights reserved.
//

import Foundation
import Alamofire
typealias ObjectJSON = [String:Any]

enum ResultJSONResponse {
    case success(ObjectJSON)
    case error(ErrorAPI)
}

enum ErrorAPI:Error{
    case invalidJSON
    case unavailableInternet
    
    var messageError:String{
        switch self {
        case .invalidJSON:
            return MessageGlossary.genericError.text
        case .unavailableInternet:
            return MessageGlossary.noInternet.text
            
            
        }
    }
}



protocol  RequestManager{
    
    typealias RequestResultDict = (_ Result:ObjectJSON?, _ RequestError:ErrorAPI?) -> Void
    typealias RequestJSONResult = (_ result: ResultJSONResponse) -> Void
    
    
    
}


class RequestManagerImp:RequestManager {

    static private let baseURL  = URL(string: Endpoint.baseURL.text)!
    private let configuration = URLSessionConfiguration.default
    var alamoFireManager : SessionManager
    
    private var headersSet: HTTPHeaders = [
        "Content-Type": "application/json",
        //"Accept": "application/vnd.twitchtv.v5+json",
        "Client-ID": Endpoint.apiKey.text
    ]
    
    
    static let shared = RequestManagerImp()
    private init(){
        configuration.timeoutIntervalForRequest = 120 // seconds
        configuration.timeoutIntervalForResource = 120
        self.alamoFireManager = Alamofire.SessionManager(configuration: configuration)
        
        
    }
    
    
    func request(with baseURL:URLConvertible, method:HTTPMethod = .get, params:Parameters?, completion:@escaping RequestResultDict){
        print(baseURL)
        if  !Reachability.isConnectedToNetwork(){
            completion(nil, ErrorAPI.unavailableInternet)
            return
        }
        alamoFireManager.request(baseURL, method: method, parameters: params, encoding: JSONEncoding.default, headers: headersSet).validate(statusCode: 200..<400).responseJSON { (response) in
            self.handleResult(response: response, completionHandler: completion)
        }
    }
   private func handleResult(response:DataResponse<Any>, completionHandler:@escaping RequestResultDict){
        switch response.result{
        case .success(let result):
            if let value = result as? [String: Any]{
                completionHandler(value, nil)
                print(value)
            } else{
                completionHandler(nil, ErrorAPI.invalidJSON )
                print(ErrorAPI.invalidJSON)
            }
        case .failure(let error):
            print(error.localizedDescription)
            completionHandler(nil, ErrorAPI.invalidJSON)
        }
    }
    
    
    
}
