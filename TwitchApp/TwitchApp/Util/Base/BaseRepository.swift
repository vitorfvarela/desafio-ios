//
//  BaseRepository.swift
//  TwitchApp
//
//  Created by Vitor Ferraz on 25/04/2018.
//  Copyright © 2018 Vitor Ferraz. All rights reserved.
//

import Foundation
protocol Repository {
    associatedtype T
    func getAll() -> [T]
    func get( identifier:Int ) -> T?

}
