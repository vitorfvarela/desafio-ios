//
//  BasePresenter.swift
//  TwitchApp
//
//  Created by Vitor Ferraz on 25/04/2018.
//  Copyright © 2018 Vitor Ferraz. All rights reserved.
//

import Foundation
protocol BasePresenter{
    associatedtype V
    var view: V { get set }
    
    associatedtype R
    var repository: R { get set }
}

