//
//  BaseView.swift
//  TwitchApp
//
//  Created by Vitor Ferraz on 25/04/2018.
//  Copyright © 2018 Vitor Ferraz. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

protocol BaseView {
    var activityData:ActivityData {get}

    func showLoading()
    func hideLoading()
    func showDefaultError()
    func showError(withMessage message:String)
    func showAlert(titleGlossary title:TitleGlossary, messageGlossary message:MessageGlossary, completion:((UIAlertAction) -> Void)?)
}

extension UIViewController:BaseView{
    
    var activityData: ActivityData {
        return ActivityData()
    }
    func showLoading() {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)

    }
    
    func hideLoading() {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()

    }
    
    func showDefaultError(){
        self.showOkAlert(title: TitleGlossary.warning.text, message: MessageGlossary.genericError.text, viewController: self, completion: nil)
    }
    func showError(withMessage message:String){
        self.showOkAlert(title: TitleGlossary.warning.text, message: message, viewController: self, completion: nil)
    }
    
    func showAlert(titleGlossary title: TitleGlossary, messageGlossary message: MessageGlossary, completion: ((UIAlertAction) -> Void)?) {
        self.showOkAlert(title: title.text, message: message.text, viewController: self, completion: completion)
        
    }
    
    func showAlert(titleGlossary title: TitleGlossary, messageGlossary message: MessageGlossary) {
        self.showOkAlert(title: title.text, message: message.text, viewController: self, completion: nil)
    }

    
}
