//
//  Constants.swift
//  TwitchApp
//
//  Created by Vitor Ferraz on 25/04/2018.
//  Copyright © 2018 Vitor Ferraz. All rights reserved.
//

import Foundation

enum Endpoint{
    case baseURL
    case apiKey
    
    var text:String{
        switch self {
        case .baseURL:
            return "https://api.twitch.tv/"
        case .apiKey:
            return "8sfslvrqcdr1s3lh2a3314pugki33l"
        }
    }
}
