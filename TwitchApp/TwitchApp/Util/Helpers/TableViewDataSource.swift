//
//  TableViewDataSource.swift
//  TwitchApp
//
//  Created by Vitor Ferraz on 25/04/2018.
//  Copyright © 2018 Vitor Ferraz. All rights reserved.
//

import UIKit

class TableViewDataSource<Cell :UITableViewCell, Model> : NSObject, UITableViewDataSource {
    
    private var cellIdentifier :String!
    private var items :[Model]!
    var configureCell :(Cell, Model) -> Void
    
    
    init(cellIdentifier :String, items :[Model], configureCell: @escaping (Cell, Model) -> Void) {
        
        self.cellIdentifier = cellIdentifier
        self.items = items
        self.configureCell = configureCell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: self.cellIdentifier, for: indexPath) as? Cell else {return UITableViewCell()}
        
        let item = self.items[indexPath.row]
        
        
        self.configureCell(cell, item)
        return cell
    }
    
}
