//
//  Glossary.swift
//  TwitchApp
//
//  Created by Vitor Ferraz on 25/04/2018.
//  Copyright © 2018 Vitor Ferraz. All rights reserved.
//

import Foundation
enum TitleGlossary{
    case warning
    case cancel
    var text:String{
        switch self {
        case .warning : return "Atenção"
        case .cancel : return "Cancelar"
        }
    }
}

enum MessageGlossary{
    case genericError
    case noInternet
    case searchGame
    
    var text:String{
        switch self {
        case .genericError: return "Ocorreu um erro. Tente novamente."
        case .noInternet : return "Por favor cheque sua cheque sua conexão e tente novamente"
        case .searchGame : return "Buscar um jogo"
        }
    }
}
