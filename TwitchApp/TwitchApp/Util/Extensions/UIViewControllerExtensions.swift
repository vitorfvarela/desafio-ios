//
//  UIViewControllerExtensions.swift
//  TwitchApp
//
//  Created by Vitor Ferraz on 25/04/2018.
//  Copyright © 2018 Vitor Ferraz. All rights reserved.
//

import UIKit

extension UIViewController{
    func showOkAlert (title:String, message:String, viewController: UIViewController, completion:((UIAlertAction) -> Void)? = nil)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: completion))
        viewController.present(alert, animated: true, completion: nil)
    }
}
