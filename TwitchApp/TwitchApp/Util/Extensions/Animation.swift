//
//  Animation.swift
//  TwitchApp
//
//  Created by Vitor Ferraz on 26/04/2018.
//  Copyright © 2018 Vitor Ferraz. All rights reserved.
//

import UIKit
class Animation{
    static func animate(cell:UITableViewCell){
        cell.transform = .init(scaleX: 0.5, y: 0.5)


        UIView.animate(withDuration: 0.75, delay: 0.0, usingSpringWithDamping: 0.75, initialSpringVelocity: 0.75, options: .curveEaseOut, animations: {
            cell.transform = .identity
            cell.transform = .init(scaleX: 1, y: 1)

            cell.alpha = 1.0
        }, completion: nil)
    }}
