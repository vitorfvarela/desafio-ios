//
//  UIViewCircle.swift
//  TwitchApp
//
//  Created by Vitor Ferraz on 27/04/2018.
//  Copyright © 2018 Vitor Ferraz. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class CustomUIView: UIView {
    
    @IBInspectable var circle: Bool = false{
        didSet{
            self.makeCircle()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.makeCircle()
        
    }
    
    func makeCircle(){
        if circle {
            layer.cornerRadius = frame.height / 2
        }
        
    }
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        self.makeCircle()
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        self.makeCircle()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
    }
    
}

class CustomUIImageView:UIImageView{
    @IBInspectable var circle: Bool = false{
        didSet{
            self.makeCircle()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.makeCircle()
        
    }
    
    func makeCircle(){
        if circle {
            layer.cornerRadius = frame.height / 2
        }
        
    }
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        self.makeCircle()
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        self.makeCircle()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
    }
    
}
@IBDesignable
class CircleImageView: UIImageView {
}
