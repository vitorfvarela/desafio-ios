//
//  Details.swift
//  TwitchApp
//
//  Created by Vitor Ferraz on 26/04/2018.
//  Copyright © 2018 Vitor Ferraz. All rights reserved.
//

import Foundation
import Tailor

struct Details: Mappable {
    var id = Int()
    var box = [String : Any]()
    var logo = [String : Any]()
    var name = String()
    var popularity = Int()
    
    
    init(_ map: [String : Any]) {
        id <- map.property("_id")
        box <- map.property("box")
        logo <- map.property("logo")
        name <- map.property("name")
        popularity <- map.property("popularity")
    }
}
