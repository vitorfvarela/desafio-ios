//
//  Game.swift
//  TwitchApp
//
//  Created by Vitor Ferraz on 25/04/2018.
//  Copyright © 2018 Vitor Ferraz. All rights reserved.
//

import Foundation
import Tailor

protocol GamePresenterCellProtocol{
    var titleCell:String {get}
    var channelCell:String {get}
    var rankCell:String {get}
    var indexCell:Int {get}
    func setupImageCell() -> URL?
}


struct Game: Mappable,GamePresenterCellProtocol {
    var titleCell: String{
        return details?.name.capitalized ?? ""
    }
    
    var channelCell: String{
        return "\(channels) Canais"
    }
    
    var rankCell: String{
        return "\(index + 1) º"
    }
    
    var indexCell: Int{
        return index
    }
    
    func setupImageCell() -> URL? {
        
        if let image = details?.box["large"] as? String, let url = URL(string: image){
            print(image)
            return url
        }
       
        return nil
    }
    
    var channels = Int()
    var viewers = Int()
    var details: Details?
    var index  = 0
    
    init(_ map: [String : Any]) {
        channels <- map.property("channels")
        viewers <- map.property("viewers")
        details <- map.relation("game")
    }
    
    mutating func setRank(rank:Int){
        index = rank
    }
}
