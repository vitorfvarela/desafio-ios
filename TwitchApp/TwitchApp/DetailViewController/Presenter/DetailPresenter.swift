//
//  DetailPresenter.swift
//  TwitchApp
//
//  Created by Vitor Ferraz on 27/04/2018.
//  Copyright © 2018 Vitor Ferraz. All rights reserved.
//

import Foundation
protocol DetailPresenterProtocol:BaseView {
    func updateWithItem(with item:Game)
    
}

class DetailPresenter:BasePresenter{
    var view: DetailPresenterProtocol
    
    var repository: GameRepository
    typealias V = DetailPresenterProtocol
    typealias R = GameRepository
    
    var game:Game
    func setupImageCell() -> URL? {
        
        if let image = game.details?.box["large"] as? String, let url = URL(string: image){
            print(image)
            return url
        }
        
        return nil
    }
    
    init(view:DetailPresenterProtocol,repository:GameRepository,game:Game) {
        self.view = view
        self.repository = repository
        self.game = game
        view.updateWithItem(with: game)

    }
    
    
    
    
    
    
    
    
}

