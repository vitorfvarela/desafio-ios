//
//  DetailViewController.swift
//  TwitchApp
//
//  Created by Vitor Ferraz on 27/04/2018.
//  Copyright © 2018 Vitor Ferraz. All rights reserved.
//

import UIKit
import Kingfisher

class DetailViewController: UIViewController ,DetailPresenterProtocol{
    func updateWithItem(with item: Game) {
        self.titleLabel.text = item.titleCell
        dump(item.titleCell)

        guard let imageURL = item.setupImageCell() else{
            self.imageCover.image =  UIImage(named: "noImage")
            return
        }
        self.imageCover.sizeToFit()
      
        imageCover.contentMode = .scaleAspectFit
        self.imageCover.kf.setImage(with: imageURL, placeholder: UIImage(named: "noImage"), options: [.transition(.fade(0.2))], progressBlock: nil, completionHandler: nil)
        self.titleLabel.text = item.titleCell
        self.chanelCountLabel.text = "\(item.channels)"
        self.viewCountLabel.text = "\(item.details?.popularity ?? 0)"
        
        var transform = self.imageCover.transform
        transform = transform.translatedBy(x: 0.0, y: 54.0)
        transform = transform.scaledBy(x: 0.01, y: 0.01)
        self.imageCover.transform = transform
        
        
        UIView.animate(withDuration: 1.0, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
             self.imageCover.transform = .identity
        }, completion: nil)
    }
    

    @IBOutlet weak var chanelCountLabel: UILabel!
    @IBOutlet weak var viewCountLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageCover: UIImageView!
    var detailPresenter:DetailPresenter!
    var game:Game!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.detailPresenter = DetailPresenter(view: self, repository: GameRepository(), game: game)

        

    }

    
    @IBAction func buyButtonPressed(_ sender: Any) {
        openSite("https://store.steampowered.com/search/?term=")

    }
    @IBAction func shareButtonPressed(_ sender: Any) {
        let text = "\("Vamos Assistir") \(game.titleCell) \("no Twitch!")"
        let activityViewController = UIActivityViewController(activityItems: [text], applicationActivities: nil)
        self.present(activityViewController, animated: true, completion: nil)
    }
    func openSite(_ baseURL: String) {
        var stringURL = "www.google.com"
        if let encodedName = detailPresenter.game.titleCell.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) {
            stringURL = baseURL + encodedName
        }
        guard let url = URL(string: stringURL) else{
            fatalError("Erro ao criar url")
        }
        UIApplication.shared.open(url, options: [:])
        
    }



    


}
